#include <iostream>
#include <string>

using namespace std;

class Animal
{

public:

    Animal(const char* word = "") : word(word) {}
    

   virtual string Voice()
   {
       std::cout << "Animal" << std::endl;

       return "Animal";
   }
    
protected:

    string word;

};

class Dogs : public Animal
{
public:
    
    Dogs (const char* word = "") : Animal(word) {}


      virtual string Voice() override
    {
          std::cout << "Woof!" << std::endl;

          return "Woof!";
    }



};


class Cats : public Dogs
{
public:

    Cats (const char* word = "") : Dogs(word){}
 
    virtual string Voice() override
    {
        std::cout << "May!" << std::endl;

        return  "May!";
    }
};

class Pigs : public Cats

{
public:

    Pigs (const char* word = "") : Cats(word) {}

    virtual string Voice() override
    {
        std::cout << "Hruu!" << std::endl;

        return  "Hruu!";
    }
};


int main()
{
    Animal* array[3];
    array[0] = new Dogs("big");
    array[1] = new Cats("small");
    array[2] = new Pigs("very big");

    for (int i = 0; i < 3; i++)
   
     cout << array[i]->Voice() << "\n";
    
}


